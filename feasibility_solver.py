from read_in import Distance
from read_in import read
from pulp import *

def CarUsageOfRequest(request, data, solvertype = 0):

    value = 1

    numberOfTools = request['numberoftools']
    toolID = request['toolid']
    toolsize = data["TOOLS"][toolID]['size']
    location1 = data["COORDINATES"][data["DEPOT_COORDINATE"]]
    location2 = data["COORDINATES"][request['location']]
    distance = Distance(location1, location2)
    capacity = data["CAPACITY"]
    maxDistance = data["MAX_TRIP_DISTANCE"]

    if(type == 0):
        # lower bound
        value = distance*numberOfTools*toolsize/(capacity*maxDistance)
    if(type == 1):
        # min number of cars
        numberOfRounds = int(numberOfTools*toolsize/capacity)+1
        if(numberOfRounds != 1):
            print("Request", request, "cannot be done in 1 round")
        value = distance*2*numberOfRounds/maxDistance

    return value

def SimpleSolver(data):

    Days = data["DAYS"]
    Capacity = data["CAPACITY"]
    MaxTripDistance = data["MAX_TRIP_DISTANCE"]
    DepotCoordinate = data["DEPOT_COORDINATE"]
    VehicleCost = data["VEHICLE_COST"]
    VehicleDayCost = data["VEHICLE_DAY_COST"]
    DistanceCost = data["DISTANCE_COST"]
    Tools = data["TOOLS"]
    Size = "size"
    NumberAvailable = "number"
    Cost = "cost"
    Coordinates = data["COORDINATES"]
    Requests = data["REQUESTS"]
    Location = "location"
    FirstDay = "firstday"
    LastDay = "lastday"
    NumberOfDays = "numberofdays"
    ToolId = "toolid"
    NumberOfTools = "numberoftools"

    result_firstdays = [[] for i in range(Days)]
    result_lastdays =  [[] for i in range(Days)]

    Problem = pulp.LpProblem("feasibility", LpMaximize)

    BasicRequests = {}  # dict of all basic requests, key is ID, value is [0,0,5,5,5,0]
    BasicRequestsPerRequests = {}  # list of basic requests structured for each requests

    RequestVariables = []
    CarVariable = []
    Costs = {}
    Constraints = {}
    # tool and day constraints [(tool, day)]
    # daily car usage constraints [day]
    # basic requests add up to requests constraints [request]
    StartingDays = {}
    EndingDays = {}

    #creating the daily tool constraints:
    for tool in Tools:
        for day in range(Days):
            DailyConstraintForTool = {}
            Constraints[(tool, day)] = DailyConstraintForTool

    #creating the daily car usage constraints
    for day in range(Days):
        DailyConstraintForCar = {}
        Constraints[day] = DailyConstraintForCar

    #creating the basic request constraints
    for request_id in Requests:
        RequestConstraint = {}
        Constraints[request_id] = RequestConstraint

    #creating all of the basic requests
    for request_id in Requests:
        request = Requests[request_id]
        basicRequestsOfRequest = []

        for day in range(request[FirstDay], request[LastDay] + 1):
            basicRequest = []
            # basicRequest contains [0,0,5,5,5,0] if 5 tools used on day 3,4,5
            basicRequestID = str(request_id) + "_" + str(day)
            RequestVariables.append(basicRequestID)
            basicRequestsOfRequest.append(basicRequestID)
            StartingDays[basicRequestID] = day
            EndingDays[basicRequestID] = day + request[NumberOfDays]

            # defining the constraints for each basicRequest
            for i in range(Days):
                if ((i + 1) >= day and (i + 1) <= (day + request[NumberOfDays])):
                    basicRequest.append(request[NumberOfTools])
                else:
                    basicRequest.append(0)

            BasicRequests[basicRequestID] = basicRequest

            Costs[basicRequestID] = 0  # the objective function will go for the car variables only
        BasicRequestsPerRequests[request_id] = basicRequestsOfRequest

    #creating car variable

    CarVariable.append("CarNumber")

    Variables = RequestVariables + CarVariable

    request_vars = pulp.LpVariable.dicts("BasicRequest", RequestVariables, cat=pulp.LpBinary)
    car_var = pulp.LpVariable.dicts("SlackVariable", CarVariable, cat=pulp.LpInteger)

    #set tool constraints
    for request_id in Requests:
        toolOfRequest = Requests[request_id]['toolid']
        for basicRequestID in BasicRequestsPerRequests[request_id]:
            for day in range(Days):
                Constraints[(toolOfRequest, day)][basicRequestID] = BasicRequests[basicRequestID][day]

    for tool in Tools:
        for day in range(Days):
            Problem += lpSum([Constraints[(tool, day)][j] * request_vars[j] for j in Constraints[(tool, day)].keys()]) <= Tools[tool]['number']



    #set request constraints
    for request_id in Requests:
        Problem += lpSum([1 * request_vars[j] for j in BasicRequestsPerRequests[request_id]]) == 1


    #set daily car constraints
    for day in range(Days):
        for request_id in Requests:
            request = Requests[request_id]
            for basicRequestID in BasicRequestsPerRequests[request_id]:
                basicRequest = BasicRequests[basicRequestID]
                if(basicRequest[day] != 0):
                    if(day == 0 or basicRequest[day-1] == 0):
                        Constraints[day][basicRequestID] = CarUsageOfRequest(request, data)
                    elif(day == (Days-1) or basicRequest[day+1] == 0):
                        Constraints[day][basicRequestID] = CarUsageOfRequest(request, data)
                    else:
                        Constraints[day][basicRequestID] = 0
                else:
                    Constraints[day][basicRequestID] = 0
        Constraints[day]["CarNumber"] = -1
        Problem += (lpSum([Constraints[day][j] * request_vars[j] for j in RequestVariables]) + lpSum([Constraints[day][j] * car_var[j] for j in CarVariable])) <= 0

    #set cost function
    for basicRequestID in RequestVariables:
        Costs[basicRequestID] = 0
    Costs["CarNumber"] = -1


    for key in Constraints.keys():
        pass
        # Problem += (lpSum([Constraints[key][j] * request_vars[j] for j in RequestVariables]) + lpSum([Constraints[key][j] * car_var[j] for j in CarVariable])) <= 0

    # objective
    Problem += pulp.lpSum([Costs[j] * car_var[j] for j in CarVariable])

    Problem.solve()
    status = LpStatus[Problem.status]

    print("Status:", status)

    if (status != "Infeasible"):
        for variable in Problem.variables():
            print(variable, variable.value())
            if variable.value() == 1.0:
                basicRequestID = variable.name.split("_", 1)[1]  # get the basic request id

                for day in range(Days):
                    if day + 1 == StartingDays[basicRequestID]:
                        result_firstdays[day].append(int(basicRequestID.split("_")[0]))
                    if day + 1 == EndingDays[basicRequestID]:
                        result_lastdays[day].append(int(basicRequestID.split("_")[0]))


    return result_firstdays, result_lastdays

def UniformSolver(data):

    Days = data["DAYS"]
    Capacity = data["CAPACITY"]
    MaxTripDistance = data["MAX_TRIP_DISTANCE"]
    DepotCoordinate = data["DEPOT_COORDINATE"]
    VehicleCost = data["VEHICLE_COST"]
    VehicleDayCost = data["VEHICLE_DAY_COST"]
    DistanceCost = data["DISTANCE_COST"]
    Tools = data["TOOLS"]
    Size = "size"
    NumberAvailable = "number"
    Cost = "cost"
    Coordinates = data["COORDINATES"]
    Requests = data["REQUESTS"]
    Location = "location"
    FirstDay = "firstday"
    LastDay = "lastday"
    NumberOfDays = "numberofdays"
    ToolId = "toolid"
    NumberOfTools = "numberoftools"

    result_firstdays = [[] for i in range(Days)]
    result_lastdays =  [[] for i in range(Days)]

    Problem = pulp.LpProblem("min_cars", LpMinimize)

    BasicRequests = {}  # dict of all basic requests, key is ID, value is [0,0,5,5,5,0]
    BasicRequestsPerRequests = {}  # list of basic requests structured for each requests

    RequestVariables = []
    CarVariable = []
    Costs = {}
    Constraints = {}
    # tool and day constraints [(tool, day)]
    # daily car usage constraints [day]
    # basic requests add up to requests constraints [request]
    StartingDays = {}
    EndingDays = {}

    #creating the daily tool constraints:
    for tool in Tools:
        for day in range(Days):
            DailyConstraintForTool = {}
            Constraints[(tool, day)] = DailyConstraintForTool

    #creating the daily car usage constraints
    for day in range(Days):
        DailyConstraintForCar = {}
        Constraints[day] = DailyConstraintForCar

    #creating the basic request constraints
    for request_id in Requests:
        RequestConstraint = {}
        Constraints[request_id] = RequestConstraint

    #creating all of the basic requests
    for request_id in Requests:
        request = Requests[request_id]
        basicRequestsOfRequest = []

        for day in range(request[FirstDay], request[LastDay] + 1):
            basicRequest = []
            # basicRequest contains [0,0,5,5,5,0] if 5 tools used on day 3,4,5
            basicRequestID = str(request_id) + "_" + str(day)
            RequestVariables.append(basicRequestID)
            basicRequestsOfRequest.append(basicRequestID)
            StartingDays[basicRequestID] = day
            EndingDays[basicRequestID] = day + request[NumberOfDays]

            # defining the constraints for each basicRequest
            for i in range(Days):
                if ((i + 1) >= day and (i + 1) <= (day + request[NumberOfDays])):
                    basicRequest.append(request[NumberOfTools])
                else:
                    basicRequest.append(0)

            BasicRequests[basicRequestID] = basicRequest

            Costs[basicRequestID] = 0  # the objective function will go for the car variables only
        BasicRequestsPerRequests[request_id] = basicRequestsOfRequest

    #creating car variable

    CarVariable.append("CarNumber")

    Variables = RequestVariables + CarVariable

    request_vars = pulp.LpVariable.dicts("BasicRequest", RequestVariables, cat=pulp.LpBinary)
    car_var = pulp.LpVariable.dicts("SlackVariable", CarVariable, cat=pulp.LpInteger)

    #set tool constraints
    for request_id in Requests:
        toolOfRequest = Requests[request_id]['toolid']
        for basicRequestID in BasicRequestsPerRequests[request_id]:
            for day in range(Days):
                Constraints[(toolOfRequest, day)][basicRequestID] = BasicRequests[basicRequestID][day]

    for tool in Tools:
        for day in range(Days):
            Problem += lpSum([Constraints[(tool, day)][j] * request_vars[j] for j in Constraints[(tool, day)].keys()]) <= Tools[tool]['number']



    #set request constraints
    for request_id in Requests:
        Problem += lpSum([1 * request_vars[j] for j in BasicRequestsPerRequests[request_id]]) == 1


    #set daily car constraints
    for day in range(Days):
        for request_id in Requests:
            request = Requests[request_id]
            for basicRequestID in BasicRequestsPerRequests[request_id]:
                basicRequest = BasicRequests[basicRequestID]
                if(basicRequest[day] != 0):
                    if(day == 0 or basicRequest[day-1] == 0):
                        Constraints[day][basicRequestID] = CarUsageOfRequest(request, data, solvertype=1)
                    elif(day == (Days-1) or basicRequest[day+1] == 0):
                        Constraints[day][basicRequestID] = CarUsageOfRequest(request, data, solvertype=1)
                    else:
                        Constraints[day][basicRequestID] = 0
                else:
                    Constraints[day][basicRequestID] = 0
        Constraints[day]["CarNumber"] = -1
        Problem += (lpSum([Constraints[day][j] * request_vars[j] for j in RequestVariables]) + lpSum([Constraints[day][j] * car_var[j] for j in CarVariable])) <= 0

    #set cost function
    for basicRequestID in RequestVariables:
        Costs[basicRequestID] = 0
    Costs["CarNumber"] = 1


    for key in Constraints.keys():
        pass
        # Problem += (lpSum([Constraints[key][j] * request_vars[j] for j in RequestVariables]) + lpSum([Constraints[key][j] * car_var[j] for j in CarVariable])) <= 0

    # objective
    Problem += pulp.lpSum([Costs[j] * car_var[j] for j in CarVariable])

    Problem.solve()
    status = LpStatus[Problem.status]

    print("Status:", status)

    if (status != "Infeasible"):
        for variable in Problem.variables():
            print(variable, variable.value())
            if variable.value() == 1.0:
                basicRequestID = variable.name.split("_", 1)[1]  # get the basic request id

                for day in range(Days):
                    if day + 1 == StartingDays[basicRequestID]:
                        result_firstdays[day].append(int(basicRequestID.split("_")[0]))
                    if day + 1 == EndingDays[basicRequestID]:
                        result_lastdays[day].append(int(basicRequestID.split("_")[0]))


    return result_firstdays, result_lastdays


'''
data = read("/AllTimeBest/VeRoLog_r100d5_1.txt")



data = read("/AllTimeBest/uniform_solver_test2.txt")
ResultFirstDays, ResultLastDays = UniformSolver(data)
print(ResultFirstDays)
print(ResultLastDays)

'''