import os
import math

def Distance(location1, location2):
    return math.pow((location2[0]-location1[0])**2 + (location2[1]-location1[1])**2,0.5)

def read(name):
    data = [x.rstrip() for x in open(os.getcwd() + name, 'r')]
    for line in data:
        #print(line)
        pass
    data_object = {}
    #print(data[3])
    data_object["DAYS"] = int(data[3].split()[2])
    #print(data[4])
    data_object["CAPACITY"] = int(data[4].split()[2])
    #print(data[5])
    data_object["MAX_TRIP_DISTANCE"] = int(data[5].split()[2])
    #print(data[6])
    data_object["DEPOT_COORDINATE"] = int(data[6].split()[2])
    #print(data[8])
    data_object["VEHICLE_COST"] = int(data[8].split()[2])
    #print(data[9])
    data_object["VEHICLE_DAY_COST"] = int(data[9].split()[2])
    #print(data[10])
    data_object["DISTANCE_COST"] = int(data[10].split()[2])

    #print(data[12])
    itemcount = int(data[12].split()[2])

    list_of_tools = []
    actual_line = 13
    dict_of_tools = {}
    for i in range(actual_line, actual_line+itemcount):
        data_of_one_tool = data[i].split()
        dict_of_one_tool = {}
        dict_of_one_tool["size"] = int(data_of_one_tool[1])
        dict_of_one_tool["number"] = int(data_of_one_tool[2])
        dict_of_one_tool["cost"] = int(data_of_one_tool[3])
        dict_of_tools[int(data_of_one_tool[0])] = dict_of_one_tool

    data_object["TOOLS"] = dict_of_tools
    actual_line += (itemcount+1)

    itemcount = int(data[actual_line].split()[2])
    actual_line += 1
    dict_of_coords = {}
    for i in range(actual_line, actual_line+itemcount):
        data_of_one_place = data[i].split()
        coordinate_x = int(data_of_one_place[1])
        coordinate_y = int(data_of_one_place[2])
        dict_of_coords[int(data_of_one_place[0])] = (coordinate_x, coordinate_y)

    data_object["COORDINATES"] = dict_of_coords
    actual_line += (itemcount+1)
    itemcount = int(data[actual_line].split()[2])
    actual_line += 1
    dict_of_requests = {}
    for i in range(actual_line, actual_line+itemcount):
        data_of_one_request = data[i].split()
        dict_of_one_request = {}
        dict_of_one_request["location"] = int(data_of_one_request[1])
        dict_of_one_request["firstday"] = int(data_of_one_request[2])
        dict_of_one_request["lastday"] = int(data_of_one_request[3])
        dict_of_one_request["numberofdays"] = int(data_of_one_request[4])
        dict_of_one_request["toolid"] = int(data_of_one_request[5])
        dict_of_one_request["numberoftools"] = int(data_of_one_request[6])
        dict_of_requests[int(data_of_one_request[0])] = dict_of_one_request
    data_object["REQUESTS"] = dict_of_requests

    #for key in data_object:
        #print(key, data_object[key])

    AddNewElements(data_object)

    return data_object

def AddNewElements(data):
    Requests = data["REQUESTS"]
    Coordinates = data["COORDINATES"]

    # add request ids and coordinates to requests
    for _id in Requests:
        Requests[_id]['id'] = int(_id)
        location = Requests[_id]['location']
        Requests[_id]['coordinate'] = Coordinates[location]