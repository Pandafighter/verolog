from read_in import read
from read_in import Distance
from feasibility_solver import SimpleSolver
import matplotlib.pyplot as plt

from pandas import DataFrame
from sklearn.cluster import KMeans
import numpy as np
import tsp


data = read("/AllTimeBest/VeRoLog_r100d5_1.txt")

Days = data["DAYS"]
Capacity = data["CAPACITY"]
MaxTripDistance = data["MAX_TRIP_DISTANCE"]
DepotCoordinate = data["DEPOT_COORDINATE"]
VehicleCost = data["VEHICLE_COST"]
VehicleDayCost = data["VEHICLE_DAY_COST"]
DistanceCost = data["DISTANCE_COST"]
Tools = data["TOOLS"]
Size = "size"
NumberAvailable = "number"
Cost = "cost"
Coordinates = data["COORDINATES"]
Requests = data["REQUESTS"]
Location = "location"
FirstDay = "firstday"
LastDay = "lastday"
NumberOfDays = "numberofdays"
ToolId = "toolid"
NumberOfTools = "numberoftools"
ID = 'id'
Coordinate = 'coordinate'

# BasicRouting creates star-like routes for each request, it is mainly for comparison
def BasicRouting(data, FirstDays, LastDays):

    outfile = open("basicRoutingSolution.txt", 'w')
    outfile.write("DATASET = ...\n")
    outfile.write("NAME = ....\n")

    totalDistance = 0
    numberOfCarsPerDay = []

    #calculate the total distance
    for dailyRequests in FirstDays:
        for i in dailyRequests:
            location = Requests[i][Location]
            totalDistance += 2 * Distance(Coordinates[DepotCoordinate],Coordinates[location])
    for dailyRequests in LastDays:
        for i in dailyRequests:
            location = Requests[i][Location]
            totalDistance += 2 * Distance(Coordinates[DepotCoordinate], Coordinates[location])

    for i in range(Days):

        outfile.write("\n")
        outfile.write("DAY = " + str(i+1) + "\n")
        tasks = FirstDays[i] + LastDays[i]
        carnumber = 1
        outfile.write(str(carnumber) + " R 0 ")
        actdistance = 0
        for j in range(len(tasks)):
            requestid = tasks[j]
            newdistance = 2 * Distance(Coordinates[DepotCoordinate], Coordinates[Requests[requestid][Location]])
            if(newdistance + actdistance > MaxTripDistance):
                carnumber += 1
                outfile.write("\n")
                outfile.write(str(carnumber) + " R 0 ")
                actdistance = newdistance
                if(j >= len(FirstDays[i])):
                    outfile.write("-" + str(requestid) + " 0 ")
                else:
                    outfile.write(str(requestid) + " 0 ")
            else:
                actdistance += newdistance
                if(j >= len(FirstDays[i])):
                    outfile.write("-" + str(requestid) + " 0 ")
                else:
                    outfile.write(str(requestid) + " 0 ")

        outfile.write("\n")



    #calculate the number of cars needed
    for first,last in zip(FirstDays, LastDays):
        numberOfCarsPerDay.append(max(len(first), len(last)))
    numberOfCars = max(numberOfCarsPerDay)
    return totalDistance, numberOfCars, numberOfCarsPerDay

# Clustering the coordinates s.t. locations within a cluster are 'close' to each other
# Also, one vehicle can carry out (and in) all of these requests

def ClusterIndicesNumpy(clustNum, labels_array): #numpy
    return np.where(labels_array == clustNum)[0]

#simple k-mean clustering, returns the set of locations in the clusters
def SimpleClustering(xCoordinates, yCoordinates, coordinateToRequestMap, nrOfClusters):
    Data = {'x': xCoordinates, 'y': yCoordinates}
    df = DataFrame(Data, columns=['x', 'y'])
    kmeans = KMeans(n_clusters=nrOfClusters).fit(df)

    #centroids = kmeans.cluster_centers_
    #plt.scatter(df['x'], df['y'], c=kmeans.labels_.astype(float), s=50, alpha=0.5)
    #plt.scatter(centroids[:, 0], centroids[:, 1], c='red', s=50)
    #plt.show()

    allCoordinates = []
    allRequests = []

    for i in range(nrOfClusters):
        #get the coordinates within a cluster
        indices = ClusterIndicesNumpy(i, kmeans.labels_)
        clusterCoordinates = df.iloc[indices].get_values().tolist()

        clusterCoordinatesTuple = []
        clusterRequests = []
        for coordinate in clusterCoordinates:
            clusterCoordinatesTuple.append(tuple(coordinate)) #we need tuples for the tsp solver algorithm
            clusterRequests.append(coordinateToRequestMap[tuple(coordinate)]) #get the request respective to the coordinate
        allCoordinates.append(clusterCoordinatesTuple)
        allRequests.append(clusterRequests)

    return allCoordinates, allRequests

#Check if a vehicle can carry out all of the items in the cluster.
def CheckClusters(data, clusteredCoordinates, clusteredRequests):
    wrongClusterCoordinates = []
    wrongClusterRequests = []
    for i in range(len(clusteredRequests)):
        weight = 0
        clusterRequests = clusteredRequests[i]
        clusterCoordinates = clusteredCoordinates[i]
        for i in range(len(clusterRequests)):
            weight += clusterRequests[i][NumberOfTools] * Tools[clusterRequests[i][ToolId]][Size]
        if weight > Capacity:
            wrongClusterRequests.append(clusterRequests)
            wrongClusterCoordinates.append(clusterCoordinates)
    return wrongClusterCoordinates, wrongClusterRequests

#Makes another clustering step, if the cluster is "too big"
def CorrectClusters(wrongClustersCoordinates, wrongClustersRequests, coordinateToRequestMap, clusteredCoordinates, clusteredRequests, numberOfClusters):
    for i in range(len(wrongClustersCoordinates)):
        clusteredCoordinates.remove(wrongClustersCoordinates[i])
        clusteredRequests.remove(wrongClustersRequests[i])
    for i in range(len(wrongClustersCoordinates)):
        requests = []
        for request in wrongClustersRequests[i]:
            requests.append(request[ID])
        xCoordinates, yCoordinates, coordinateToRequestMap = GetCoordinatesToCluster(requests)
        if len(xCoordinates) != 0:
            clusteredCoordinatesInner, clusteredRequestsInner = SimpleClustering(xCoordinates, yCoordinates, coordinateToRequestMap, 2) # divide the cluster by two
        clusteredCoordinates += clusteredCoordinatesInner
        clusteredRequests += clusteredRequestsInner
    wrongClustersCoordinates.clear()
    wrongClustersRequests.clear()


def GetCoordinatesToCluster(dailyRequests):
    xCoordinatesToCluster = []
    yCoordinatesToCluster = []
    coordinateToRequestMap = {}
    for i in dailyRequests:
        location = Requests[i][Location]
        coordinate = list(Coordinates[location])
        coordinateToRequestMap[tuple(coordinate)] = Requests[i] #hope we get every coordinate only once
        xCoordinatesToCluster.append(coordinate[0])
        yCoordinatesToCluster.append(coordinate[1])
    return xCoordinatesToCluster, yCoordinatesToCluster, coordinateToRequestMap

def GetDailyRoutes(clusteredCoordinates, numberOfClusters):
    for i in range(numberOfClusters):
        # add the depot coordinate to each cluster, and then calculate the Hamiltonian cycle
        clusteredCoordinates[i].append(Coordinates[DepotCoordinate])
        t = tsp.tsp(clusteredCoordinates[i])
        return t

def GetRoutes(FirstOrLastDays, outfile, previousCars):
    totalDistance = 0
    numberOfCarsPerDay = []
    dayCount = 0
    for dailyRequests in FirstOrLastDays:
        carCount = 0
        if(previousCars != 0):
            carCount += previousCars[dayCount]
        dayCount += 1
        outfile.write("\n")
        outfile.write("DAY = " + str(dayCount) + "\n")

        xCoordinates, yCoordinates, coordinateToRequestMap = GetCoordinatesToCluster(dailyRequests)
        numberOfClusters = 2
        if len(xCoordinates) != 0: #there are some coordinates to cluster
            clusteredCoordinates, clusteredRequests = SimpleClustering(xCoordinates, yCoordinates, coordinateToRequestMap, numberOfClusters)

            wrongClustersCoordinates, wrongClustersRequests = CheckClusters(data, clusteredCoordinates, clusteredRequests)
            while(len(wrongClustersCoordinates) != 0):
                CorrectClusters(wrongClustersCoordinates, wrongClustersRequests, coordinateToRequestMap, clusteredCoordinates, clusteredRequests, numberOfClusters)
                wrongClustersCoordinates, wrongClustersRequests = CheckClusters(data, clusteredCoordinates, clusteredRequests)
            numberOfClusters = len(clusteredCoordinates)
            print("day", dayCount, clusteredCoordinates)
            for task in range(len(clusteredRequests)):
                carCount += 1
                outfile.write(str(carCount) + " R 0 ")
                for taskRequest in clusteredRequests[task]:
                    #print(taskCoordinate)
                    for request_id in Requests:
                        if(Requests[request_id] == taskRequest):
                            #print(location_id, Coordinates[location_id])
                            if(previousCars != 0):
                                outfile.write("-")
                            outfile.write(str(request_id) + " ")
                outfile.write("0\n")

            t = GetDailyRoutes(clusteredCoordinates, numberOfClusters)
            totalDistance += t[0]
            numberOfCarsPerDay.append(numberOfClusters)
        else: #no points
            numberOfCarsPerDay.append(0)

    numberOfCars = sum(numberOfCarsPerDay)
    return totalDistance, numberOfCars, numberOfCarsPerDay

def ClusteredRouting(data, FirstDays, LastDays):

    outfile = open("clusteredRoutingSolution.txt", 'w')
    outfile.write("DATASET = ...\n")
    outfile.write("NAME = ....\n")

    totalDistanceFirst, numberofCarsFirst, numberOfCarsPerDayFirst = GetRoutes(FirstDays, outfile=outfile, previousCars=0)
    print("First:")
    print(totalDistanceFirst, numberOfCarsPerDayFirst)

    totalDistanceLast, numberofCarsLast, numberOfCarsPerDayLast = GetRoutes(LastDays, outfile=outfile, previousCars=numberOfCarsPerDayFirst)
    print("Last:")
    print(totalDistanceLast, numberOfCarsPerDayLast)

    # find the total distance, etc.
    totalDistance = totalDistanceFirst+totalDistanceLast
    numberOfCars = 0
    numberOfCarsPerDay = []
    for i in range(len(numberOfCarsPerDayFirst)):
        numberOfCarsPerDay.append(max(numberOfCarsPerDayFirst[i], numberOfCarsPerDayLast[i]))
        if(numberOfCarsPerDay[i] > numberOfCars):
            numberOfCars = numberOfCarsPerDay[i]

    return totalDistance, numberOfCars, numberOfCarsPerDay
