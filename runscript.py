from read_in import read
from feasibility_solver import SimpleSolver
from feasibility_solver import UniformSolver
from simple_solution import simple_solution
from routing_optimization import BasicRouting
from routing_optimization import ClusteredRouting
from vrpspd_solver import SolveVRPSPD

#simple_solution("/AllTimeBest/VeRoLog_r100d5_1.txt")

#data = read("/AllTimeBest/simpledata.txt")
first_problem = "/AllTimeBest/VeRoLog_r100d5_1.txt"
first_cutted = "/AllTimeBest/verolog_1_cutted.txt"
simple_problem = "/AllTimeBest/uniform_solver_test.txt"
first_ortec = "/ORTEC_Test/ORTEC_Test_04.txt"

data = read(first_problem)

ResultFirstDays, ResultLastDays = UniformSolver(data)
print(ResultFirstDays)
print(ResultLastDays)

#SolveVRPSPD(data, ResultFirstDays, ResultLastDays)

# nothing has changed


#basic solution
totalDistance, numberOfCars, numberOfCarsPerDay = BasicRouting(data, ResultFirstDays, ResultLastDays)
print("BasicRouting:")
print("  length: ", totalDistance)
print("  number of total cars needed: ", numberOfCars)
print("  number of total cars needed per days: ", numberOfCarsPerDay)

#clustered solution
totalDistance2, numberOfCars2, numberOfCarsPerDay2 = ClusteredRouting(data, ResultFirstDays, ResultLastDays)
print("ClusteredRouting:")
print("  length: ", totalDistance2)
print("  number of total cars needed: ", numberOfCars2)
print("  number of total cars needed per days: ", numberOfCarsPerDay2)
'''
'''