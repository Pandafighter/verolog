from read_in import read
from read_in import Distance


def CheckBasicFeasibility(name):

    data = read(name)
    Days = data["DAYS"]
    Capacity = data["CAPACITY"]
    MaxTripDistance = data["MAX_TRIP_DISTANCE"]
    DepotCoordinate = data["DEPOT_COORDINATE"]
    VehicleCost = data["VEHICLE_COST"]
    VehicleDayCost = data["VEHICLE_DAY_COST"]
    DistanceCost = data["DISTANCE_COST"]
    Tools = data["TOOLS"]
    Size = "size"
    NumberAvailable = "number"
    Cost = "cost"
    Coordinates = data["COORDINATES"]
    Requests = data["REQUESTS"]
    Location = "location"
    FirstDay = "firstday"
    LastDay = "lastday"
    NumberOfDays = "numberofdays"
    ToolId = "toolid"
    NumberOfTools = "numberoftools"
    max_dist = 0
    for i in Requests:
        for j in Requests:
            dist = Distance(Coordinates[Requests[i][Location]], Coordinates[Requests[j][Location]])
            if (max_dist < dist):
                max_dist = dist
    max_tool_size = 0;
    for i in Tools:
        size = Tools[i][Size]
        if (max_tool_size < size):
            max_tool_size = size
    if (max_dist > MaxTripDistance or max_tool_size > Capacity):
        return False
    return True


def simple_solution(name):

    data = read(name)
    Days = data["DAYS"]
    Capacity = data["CAPACITY"]
    MaxTripDistance = data["MAX_TRIP_DISTANCE"]
    DepotCoordinate = data["DEPOT_COORDINATE"]
    VehicleCost = data["VEHICLE_COST"]
    VehicleDayCost = data["VEHICLE_DAY_COST"]
    DistanceCost = data["DISTANCE_COST"]
    Tools = data["TOOLS"]
    Size = "size"
    NumberAvailable = "number"
    Cost = "cost"
    Coordinates = data["COORDINATES"]
    Requests = data["REQUESTS"]
    Location = "location"
    FirstDay = "firstday"
    LastDay = "lastday"
    NumberOfDays = "numberofdays"
    ToolId = "toolid"
    NumberOfTools = "numberoftools"

    all_distance = 0
    max_cars_per_day = 0 #if 4 cars used on day 1 and 3 on day 2 then this value is 4
    all_cars_and_day = 0 #if 4 cars used on day 1 and 3 on day 2 then this value is 7

    #check the basic feasibility conditions

    if(not(CheckBasicFeasibility(name))):
        print("No feasible solution")
        return

    for day in range(Days):

        daily_used_cars = 1
        distance_of_last_car = 0
        daily_distance = 0
        daily_requests_send = [request_id for request_id in Requests.keys() if Requests[request_id][FirstDay] == (1+day)]
        daily_requests_receive = [request_id for request_id in Requests.keys()
                                  if (Requests[request_id][FirstDay] + Requests[request_id][NumberOfDays]) == (1+day)]
        print(daily_requests_send)
        print(daily_requests_receive)

        daily_requests = daily_requests_send + daily_requests_receive

        for request_id in daily_requests:

            location1 = Coordinates[Requests[request_id][Location]]
            location2 = Coordinates[DepotCoordinate]

            request_distance = 2 * Distance(location1, location2)

            #calculate the tool weight

            id = Requests[request_id][ToolId]
            nr = Requests[request_id][NumberOfTools]
            weight = Tools[id][Size]

            total_weight = nr*weight

            if(distance_of_last_car + request_distance <= MaxTripDistance and total_weight <= Capacity):
                #this car can make the trip
                distance_of_last_car += request_distance
                daily_distance += request_distance
            elif(distance_of_last_car + request_distance > MaxTripDistance ):
                #a new car is necessary
                daily_used_cars += 1
                distance_of_last_car = request_distance
            elif(total_weight > Capacity):
                # tool deliver has to be splitted
                temp_weight = 0
                for i in nr:
                    if(temp_weight > Capacity):
                        break
                    temp_weight += weight
                daily_used_cars += total_weight//temp_weight+1

        all_distance += daily_distance
        if(daily_distance == 0):
            #no work at this day
            daily_used_cars = 0

        all_cars_and_day += daily_used_cars
        if(daily_used_cars > max_cars_per_day):
            max_cars_per_day = daily_used_cars

        print("daily used cars: ", daily_used_cars)

    all_cost = max_cars_per_day * VehicleCost
    all_cost += all_cars_and_day * VehicleDayCost
    all_cost += all_distance * DistanceCost

    print("All Costs: ", all_cost)




#print("test of distance function: ", distance((0,0), (3,4)))