from read_in import Distance
from read_in import read
from feasibility_solver import SimpleSolver
from pulp import *


def SolveVRPSPD(data, FirstDays, LastDays):

    Days = data["DAYS"]
    Capacity = data["CAPACITY"]
    MaxTripDistance = data["MAX_TRIP_DISTANCE"]
    DepotCoordinate = data["DEPOT_COORDINATE"]
    VehicleCost = data["VEHICLE_COST"]
    VehicleDayCost = data["VEHICLE_DAY_COST"]
    DistanceCost = data["DISTANCE_COST"]
    Tools = data["TOOLS"]
    Size = "size"
    NumberAvailable = "number"
    Cost = "cost"
    Coordinates = data["COORDINATES"]
    Requests = data["REQUESTS"]
    Location = "location"
    FirstDay = "firstday"
    LastDay = "lastday"
    NumberOfDays = "numberofdays"
    ToolId = "toolid"
    NumberOfTools = "numberoftools"
    ID = 'id'
    Coordinate = 'coordinate'

    for day in range(Days):
        print("Day ", day)
        Problem = pulp.LpProblem("VRPSPD_solver", LpMinimize)

        #setting J,V,D,P
        ''' J: set of locations
            N: set of all nodes in the graph, here N = J U {depot}
            V: set of vehicles
            D: set of demands for each location
            P: set of supplies for each location'''

        J = []
        D = {}
        D_overload = {}
        P = {}
        P_overload = {}

        for i, requestId in enumerate(FirstDays[day]):
            locationId = Requests[requestId][Location]
            toolId = Requests[requestId][ToolId]
            numberOfTools = Requests[requestId][NumberOfTools]
            toolSize = Tools[toolId][Size]

            weight = toolSize*numberOfTools

            if not (locationId in J): #this location is not in the list J yet
                J.append(locationId)
                D[locationId] = weight
                P[locationId] = 0
                D_overload[locationId] = 0
                P_overload[locationId] = 0
            else: #the location is already in J
                if weight + D[10000*D_overload[locationId]+locationId] <= Capacity: #we do not extend the car capacity
                    D[10000*D_overload[locationId]+locationId] += weight
                else: #we have to set a new location ID
                    D_overload[locationId] += 1
                    J.append(10000*D_overload[locationId]+locationId)
                    D[10000*D_overload[locationId]+locationId] = weight
                    P[10000*D_overload[locationId]+locationId] = 0
                    D_overload[10000 * D_overload[locationId] + locationId] = 0
                    P_overload[10000 * D_overload[locationId] + locationId] = 0


        for i, requestId in enumerate(LastDays[day]):
            locationId = Requests[requestId][Location]
            toolId = Requests[requestId][ToolId]
            numberOfTools = Requests[requestId][NumberOfTools]
            toolSize = Tools[toolId][Size]

            weight = toolSize * numberOfTools

            if not (locationId in J):
                J.append(locationId)
                P[locationId] = weight
                D[locationId] = 0
                D_overload[locationId] = 0
                P_overload[locationId] = 0
            else:
                if weight + P[10000*P_overload[locationId]+locationId] <= Capacity:
                    P[10000*D_overload[locationId]+locationId] += weight
                else:
                    P_overload[locationId] += 1
                    J.append(10000 * P_overload[locationId] + locationId)
                    D[10000 * P_overload[locationId] + locationId] = 0
                    P[10000 * P_overload[locationId] + locationId] = weight
                    D_overload[10000 * P_overload[locationId] + locationId] = 0
                    P_overload[10000 * P_overload[locationId] + locationId] = 0

        N = [DepotCoordinate] + J

        V = list(range(len(J))) # TODO: set an upper bound for the number of vehicles

        #defining the variables
        x_variables = pulp.LpVariable.dicts("x", (N,N,V), cat=pulp.LpBinary)
        z_variables = pulp.LpVariable.dicts("z", J, lowBound = 0, cat=pulp.LpInteger)
        L_variables = pulp.LpVariable.dicts("L", J, upBound = Capacity, lowBound = 0, cat=pulp.LpInteger)

        #distance function
        distance = {}
        for i in N:
            real_i = i % 10000
            location1 = Coordinates[real_i]
            for j in N:
                real_j = j % 10000
                location2 = Coordinates[real_j]
                distance[(i, j)] = Distance(location1, location2)

        #setting M to be sufficiently big
        M = max(sum([D[i]+P[i] for i in J]), sum([distance[(i,j)] for i in N for j in N]))

        #constraints
        print("(extra1)")
        for i in N:
            for v in V:
                Problem += x_variables[i][i][v] == 0
        print("(11)")
        for i in J:
            Problem += pulp.lpSum([x_variables[i][j][v] for j in N for v in V]) == 1
        print("(12)")
        for j in J:
            Problem += pulp.lpSum([x_variables[i][j][v] for i in N for v in V]) == 1
        print("(13)")
        for k in J:
            for v in V:
                Problem += pulp.lpSum([x_variables[i][k][v] for i in N]) - pulp.lpSum([x_variables[k][j][v] for j in N]) == 0
        print("(14)")
        for j in J:
            for v in V:
                Problem += pulp.lpSum([D[k]*x_variables[i][k][v] for i in N for k in J])-D[j]+P[j]-M*(1-x_variables[0][j][v]) <= L_variables[j]
        print("(15)")
        for i in J:
            for j in J:
                if i != j:
                    Problem += L_variables[i]-D[j]+P[j]-M*(1-pulp.lpSum([x_variables[i][j][v] for v in V])) <= L_variables[j]
        print("(16)")
        for v in V:
            Problem += pulp.lpSum([D[j]*x_variables[i][j][v] for i in N for j in J]) <= Capacity
        print("(18)")
        for i in J:
            for j in J:
                if i != j:
                    Problem += z_variables[i]+1-len(J)*(1-pulp.lpSum([x_variables[i][j][v] for v in V])) <= z_variables[j]
        print("(extra2)")
        for v in V:
            Problem += pulp.lpSum([distance[(i, j)] * x_variables[i][j][v]] for i in N for j in N) <= MaxTripDistance
        #objective function
        print("(10)")
        x_var_set = [(i, j, v) for i in N for j in N for v in V]
        Problem += pulp.lpSum([distance[(i,j)]*x_variables[i][j][v]] for (i,j,v) in x_var_set)

        Problem.solve()
        status = LpStatus[Problem.status]

        print("Status:", status)

        if (status != "Infeasible"):
            for variable in Problem.variables():
                print(variable, variable.value())
                print(type(variable))


